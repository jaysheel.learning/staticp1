package com.amdocs;

import static org.junit.Assert.*;
import org.junit.Test;

public class BasicTest {

    @Test
    public void testAdd() throws Exception {
        final int val= new Calculator().add();
        assertEquals("Add", 9, val);
        
    }

    @Test   
    public void testSub() throws Exception {
        final int val= new Calculator().sub();
        assertEquals("Sub", 3, val);

    }

    @Test
    public void testCounter() throws Exception {
        final int val= new Increment().getCounter();
        assertEquals("GetCounter", 1, val);
    }

    @Test
    public void testdecreasecounterScn1() throws Exception {
        final int val= new Increment().decreasecounter(1);
        assertEquals("Decrease Counter for 1", 1, val);
    }

    @Test
    public void testdecreasecounterScn2() throws Exception {
        final int val= new Increment().decreasecounter(0);
        assertEquals("Decrease Counter for 0", 1, val);
    }


    @Test
    public void testdecreasecounterScn3() throws Exception {
        final int val= new Increment().decreasecounter(2);
        assertEquals("Decrease Counter for 1", 1, val);
    }

}
